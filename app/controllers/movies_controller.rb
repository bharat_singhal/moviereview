class MoviesController < ApplicationController

	before_action :find_movie, only: [:edit, :show, :update]
	def new
		@movie = Movie.new
	end

	def edit
		
	end

	def create
		@movie = Movie.new(movie_params)
		if @movie.save 
			redirect_to @movie, notice: "Successfully created the movie"
		else
		    render 'new' 
		end 
	end

	def destroy
		
	end

	def update
		if @movie.update(movie_params)
			redirect_to @movie, notice: "Successfully updated the movie"
		else
		  render 'edit'
		end   

	end

	def index
	  if params[:search]
	  	@movies = Movie.search params[:search]
	  else 
	    @movies = Movie.all
	  end

	end

	def show
	  rating_array = @movie.all_ratings
	  @rating_avg = rating_avg(rating_array)
	  @reviews = @movie.reviews
	  puts "Reviews size ***********: #{@reviews.size}***************"
	end
    
    def search_movie
    	
    end
	private

	def rating_avg(rating_array)
		rating_array.inject(0.0){ |sum, el| sum + el }.to_f / rating_array.size
		
	end

	def find_movie
		@movie = Movie.find(params[:id])
		
	end

	def movie_params
		params.require(:movie).permit(:title, :description, :rating, :director, :image)
	end
end
