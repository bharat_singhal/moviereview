class ReviewsController < ApplicationController
  before_action :set_movie


	def new
		#@review = current_user.reviews.build
		
		#@review = @movie.reviews.new
	end

	def create
		
		@review = @movie.reviews.new(review_params)
		@review.user = current_user
		rating_temp = @movie.ratings.new
		rating_temp.user = current_user
		rating_temp.rating = params[:score]
		
		if @review.save && rating_temp.save
			redirect_to :back, notice: "Review added successfully !"
		else 
			render :back
		end 
	end


	private 

	def set_movie
		@movie = Movie.find(params[:movie_id])
	end

	def review_params
		params.require(:review).permit(:review_text)
	end

	
end
