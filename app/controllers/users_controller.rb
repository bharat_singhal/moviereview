class UsersController < ApplicationController
	def user
		@users = User.all_except_current_user(current_user)
	end
end
