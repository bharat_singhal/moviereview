module MoviesHelper
	def current_user_commented?(movie)
		current_user.commented?(movie)
		
	end

	def rating_wrt_review(movie,review)
	  movie.get_rating_for_review(review)
	end
end
