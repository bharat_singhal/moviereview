class Movie < ActiveRecord::Base
  searchkick
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  has_many :reviews, dependent: :destroy
  has_many :ratings, dependent: :destroy
  #ratyrate_rateable :movie_rating

  def all_ratings
  	ratings.pluck(:rating)
  end

  def get_rating_for_review(review)
    user = ratings.find_by(user_id: review.user_id)
    if user
      user.rating
    else

    end  
  end
end
