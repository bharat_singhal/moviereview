class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :reviews, dependent: :destroy

  has_many :ratings, dependent: :destroy
  has_many :conversations, foreign_key: :sender_id

  scope :all_except_current_user, -> (current_user){  where("id != ?", current_user.id) }

  #ratyrate_rater


  def commented?(movie)
    ratings.find_by(movie_id: movie.id)
  	
  end
end
